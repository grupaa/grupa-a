package grupa.a.taskify.utils;


import javafx.scene.control.TextField;

public class Validator {
    private Validator(){}

    public static boolean validateIsLongerThan(int length, TextField... textFields ) {
        boolean isValid = true;
        for (TextField tf : textFields) {
            if (tf.getText().length()< length) {
                isValid = false;
                tf.setStyle("-fx-border-color: red");
            }
        }
        return isValid;
    }
}
