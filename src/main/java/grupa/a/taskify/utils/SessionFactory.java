package grupa.a.taskify.utils;

import grupa.a.taskify.model.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class SessionFactory {

    private static final org.hibernate.SessionFactory ourSessionFactory;
    private static final ServiceRegistry serviceRegistry;

    private SessionFactory() {
    }

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            configuration.addAnnotatedClass(TaskifyUser.class);
            configuration.addAnnotatedClass(Notification.class);
            configuration.addAnnotatedClass(Board.class);
            configuration.addAnnotatedClass(Task.class);
            configuration.addAnnotatedClass(Taskmanagers.class);
            configuration.addAnnotatedClass(Group.class);
            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            ourSessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Exception ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

}
