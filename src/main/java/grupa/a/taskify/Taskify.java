package grupa.a.taskify;

import grupa.a.taskify.model.Group;
import grupa.a.taskify.model.TaskifyUser;
import grupa.a.taskify.utils.Hash;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.controlsfx.dialog.ProgressDialog;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Główna klasa programu
 */
public class Taskify extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Metoda rozpoczynająca działanie programu
     * pierwszym widokiem jest Login
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
        Scene scene = new Scene(root);
        primaryStage.setOnCloseRequest(e -> System.exit(0));
        primaryStage.setTitle("Taskify");
        primaryStage.setScene(scene);
        primaryStage.show();
        Task<Object> worker = new Task<Object>() {
            @Override
            protected Object call() throws Exception {
                try (Session session = grupa.a.taskify.utils.SessionFactory.getSession()){
                    byte[] salt = Hash.getNextSalt();
                    TaskifyUser first  = new TaskifyUser("user", Hash.hash("user".toCharArray(), salt),"Name", "surname", salt);
                    Group group = new Group();
                    group.setGroupname("Grupa1");
                    Group group2 = new Group();
                    group2.setGroupname("Grupa2");
                    first.addGroup(group);
                    first.addGroup(group2);
                    TaskifyUser second  = new TaskifyUser("user2", Hash.hash("user2".toCharArray(), salt),"Name", "surname", salt);
                    second.addGroup(group2);
                    Transaction tx = session.beginTransaction();
                    session.save(first);
                    session.save(second);
                    tx.commit();
                }
                return null;
            }

        };
        ProgressDialog dlg = new ProgressDialog(worker);
        Thread th = new Thread(worker);
        th.setDaemon(true);
        th.start();
    }
}
