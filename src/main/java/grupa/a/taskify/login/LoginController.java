package grupa.a.taskify.login;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import grupa.a.taskify.authentication.Authenticator;
import grupa.a.taskify.model.TaskifyUser;
import grupa.a.taskify.utils.Hash;
import grupa.a.taskify.utils.SessionFactory;
import grupa.a.taskify.utils.Validator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.io.IOException;

public class LoginController {
    @FXML

    private Button loginButton;
    @FXML
    private JFXTextField loginText;
    @FXML
    private JFXPasswordField passText;

    @FXML
    protected void login(ActionEvent event) throws IOException {


        if (Validator.validateIsLongerThan(0, loginText, passText)) {
            try (Session session = SessionFactory.getSession()) {
                TaskifyUser taskifyUser = (TaskifyUser) session.createCriteria(TaskifyUser.class).add(
                        Restrictions.eq("username", loginText.getText())).uniqueResult();
                if (Hash.isExpectedPassword(passText.getText().toCharArray(), taskifyUser.getSalt(),
                        taskifyUser.getPassword())) {
                    Authenticator.getInstance().setUser(taskifyUser);
                    switchSceneToTaskView(event);
                } else {
                    loginText.setStyle("-fx-border-color: red");
                    passText.setStyle("-fx-border-color: red");
                }
            }
        }
    }

    @FXML
    protected void switchSceneToRegister(ActionEvent actionEvent) throws IOException {
        Parent registerSceneParent = FXMLLoader.load(getClass().getResource("/GUI/Registry.fxml"));
        Scene registerScene = new Scene(registerSceneParent);
        Stage mainStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        mainStage.setScene(registerScene);
        mainStage.show();
    }

    private void switchSceneToTaskView(ActionEvent event) throws IOException {
        Parent taskViewSceneParent = FXMLLoader.load(getClass().getResource("/GUI/MainViev.fxml"));
        Scene taskViewScene = new Scene(taskViewSceneParent);
        Stage mainStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        mainStage.setScene(taskViewScene);
        mainStage.show();
    }
}
