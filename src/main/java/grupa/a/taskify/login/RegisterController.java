package grupa.a.taskify.login;


import grupa.a.taskify.model.TaskifyUser;
import grupa.a.taskify.utils.Hash;
import grupa.a.taskify.utils.SessionFactory;
import grupa.a.taskify.utils.Validator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.io.IOException;
import java.util.List;


/**
 * Created by c2q on 2016-03-14.
 */
public class RegisterController {
    @FXML
    private Button registerButton;
    @FXML
    private TextField loginText;
    @FXML
    private TextField passText;
    @FXML
    private TextField passText2;
    @FXML
    private TextField loginName;
    @FXML
    private TextField loginSurname;


    @FXML
    protected void register(ActionEvent event) throws IOException {


        if (Validator.validateIsLongerThan(4, loginText, passText, passText2)) {
            if (passText.getText().equals(passText2.getText())) {
                try (Session session = SessionFactory.getSession()) {
                    List results = session.createCriteria(TaskifyUser.class).add(
                            Restrictions.eq("username", loginText.getText())).setMaxResults(1).list();
                    if (results.isEmpty()) {
                        Transaction tx;
                        tx = session.beginTransaction();
                        byte[] salt = Hash.getNextSalt();
                        TaskifyUser taskifyUser = new TaskifyUser(loginText.getText(),
                                Hash.hash(passText.getText().toCharArray(), salt),
                                loginName.getText(), loginSurname.getText(), salt);
                        session.save(taskifyUser);
                        tx.commit();
                        switchSceneToLogin(event);

                    }
                }
            } else {
                passText.setStyle("-fx-background-color: red;");
                passText2.setStyle("-fx-background-color: red;");
            }


        }
    }

    public void switchSceneToLogin(ActionEvent actionEvent) throws IOException {
        Parent loginSceneParent = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
        Scene loginScene = new Scene(loginSceneParent);
        Stage mainStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        mainStage.setScene(loginScene);
        mainStage.show();
    }
}
