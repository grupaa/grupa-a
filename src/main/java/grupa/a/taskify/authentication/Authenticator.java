package grupa.a.taskify.authentication;

import grupa.a.taskify.model.TaskifyUser;


public class Authenticator {
    private static Authenticator instance = null;
    private TaskifyUser user;

    public static Authenticator getInstance()

    {
        if (instance == null) {
            instance = new Authenticator();
        }
        return instance;
    }

    public TaskifyUser getUser() {
        return user;
    }

    public void setUser(TaskifyUser user) {
        this.user = user;
    }

}
