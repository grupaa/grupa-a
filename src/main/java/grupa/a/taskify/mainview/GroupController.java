package grupa.a.taskify.mainview;

import grupa.a.taskify.authentication.Authenticator;
import grupa.a.taskify.model.Group;
import grupa.a.taskify.model.TaskifyUser;
import grupa.a.taskify.utils.SessionFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.io.IOException;
import java.util.List;

/**
 * Created by c2q on 2016-03-23.
 */
public class GroupController {
    @FXML

    private Button groupButton;
    @FXML
    private TextField groupText;

 public void addGroup (ActionEvent event) throws IOException{
     try (Session session = SessionFactory.getSession()) {
         List results = session.createCriteria(Group.class).add(
                 Restrictions.eq("groupname", groupText.getText())).setMaxResults(1).list();
         if(groupText.getText().length()>4){
         if (results.isEmpty() ) {
             Transaction tx;
             tx = session.beginTransaction();

             TaskifyUser first = Authenticator.getInstance().getUser();
             Group group = new Group();
             group.setGroupname(groupText.getText());
             first.addGroup(group);

             session.save(first);
             tx.commit();

         }else
         {
             Notifications.create()
                     .text("Grupa o takiej nazwie już istenieje.Wpisz nową nazwe grupy")
                     .darkStyle()
                     .hideCloseButton()
                     .position(Pos.CENTER)
                     .hideAfter(Duration.seconds(2))
                     .showWarning();

         }}else {
             Notifications.create()
                     .text("Za krótka nazwa grupy")
                     .darkStyle()
                     .hideCloseButton()
                     .position(Pos.CENTER)
                     .hideAfter(Duration.seconds(2))
                     .showWarning();
         }
         }
     }  }

