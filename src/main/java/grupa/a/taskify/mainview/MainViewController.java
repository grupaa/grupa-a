package grupa.a.taskify.mainview;

import grupa.a.taskify.authentication.Authenticator;
import grupa.a.taskify.model.Group;
import grupa.a.taskify.model.TaskifyUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

public class MainViewController implements Initializable, Observer{
    @FXML
    private Pane changingPane;
    @FXML
    private TreeView listTree;

    private TaskifyUser taskifyUser = Authenticator.getInstance().getUser();
    public void manageGroups(ActionEvent actionEvent) throws IOException {
        changingPane.getChildren().clear();
        changingPane.getChildren().add(FXMLLoader.load(getClass().getResource("/GUI/GroupAdder.fxml")));

    }

    public void manageTasks(ActionEvent actionEvent) throws IOException {
        changingPane.getChildren().clear();
        changingPane.getChildren().add(FXMLLoader.load(getClass().getResource("/GUI/TaskAdder.fxml")));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TreeItem<String> root = new TreeItem<>("Grupy");
        root.setExpanded(true);
        TaskifyUser taskify = Authenticator.getInstance().getUser();
        for(Group group : grupa.a.taskify.authentication.Authenticator.getInstance().getUser().getGroups()) {
            root.getChildren().add(new TreeItem<>(group.getGroupname()));
        }
        Authenticator.getInstance().getUser().addObserver(this);
    listTree.setRoot(root);
    }

    @Override
    public void update(Observable o, Object arg) {
        taskifyUser = (TaskifyUser) o;
        listTree.setRoot(null);
        TreeItem<String> root = new TreeItem<>("Grupy");
        root.setExpanded(true);
        TaskifyUser taskify = Authenticator.getInstance().getUser();
        for(Group group : grupa.a.taskify.authentication.Authenticator.getInstance().getUser().getGroups()) {
            root.getChildren().add(new TreeItem<>(group.getGroupname()));
        }
        listTree.setRoot(root);
    }
}
