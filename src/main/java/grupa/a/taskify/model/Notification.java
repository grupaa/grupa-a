package grupa.a.taskify.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Kazik on 18.03.2016.
 */
@Entity
public class Notification {
    @Column(name = "chcked")
    private String checked;
    @Column(name = "content")
    private String content;
    @Column(name = "date")
    private Date date;
    @Id
    @GeneratedValue

    @Column(name = "notificationid")
    private long notificationid;
    @ManyToOne
    @JoinColumn(name = "taskifyuserid")
    private TaskifyUser taskifyUser;

    public Notification() {


    }

    public Notification(String checked, String content, Date date) {
        this.checked = checked;
        this.content = content;
        this.date = date;
    }

    public long getNotificationid() {
        return notificationid;
    }

    public void setNotificationid(long notificationid) {
        this.notificationid = notificationid;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
