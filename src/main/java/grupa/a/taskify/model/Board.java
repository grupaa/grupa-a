package grupa.a.taskify.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Kazik on 18.03.2016.
 */
@Entity
public class Board {
    @Column(name = "name")
    private String name;
    @Id
    @GeneratedValue

    @Column(name = "boardid")
    private long boardid;
    @ManyToOne
    @JoinColumn(name = "groupid")
    private Group group;

    @OneToMany(mappedBy = "board")
    private Set<Task> task;

    public Board() {
        // No argument constructor required by Hibernate
    }

    public Board(String name) {
        this.name = name;
    }

    public long getBoardId() {
        return boardid;
    }

    public void setBoardId(long boardid) {
        this.boardid = boardid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
