package grupa.a.taskify.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Kazik on 18.03.2016.
 */
@Entity
public class Taskmanagers {
    @Id
    @GeneratedValue
    @Column(name = "taskmanagerid")
    private long taskmanagerid;

    public Taskmanagers() {
        // No argument constructor required by Hibernate
    }

    public long getTaskmanagerid() {
        return taskmanagerid;
    }

    public void setTaskmanagerid(long taskmanagerid) {
        this.taskmanagerid = taskmanagerid;
    }
}
