package grupa.a.taskify.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Kazik on 14.03.2016.
 */
@Entity
public class Task {
    @Id
    @GeneratedValue

    @Column(name = "taskid")
    private long taskid;

    @Column(name = "content")
    private String content;

    @Column(name = "isDone")
    private String isDone;

    @Column(name = "date")
    private Date date;

    @ManyToOne
    @JoinColumn(name = "boardid")
    private Board board;

    public Task() {
        // No argument constructor required by Hibernate

    }

    public Task(String content, String isDone, Date date) {
        this.content = content;
        this.isDone = isDone;
        this.date = date;
    }

    public long getTaskid() {
        return taskid;
    }

    public void setTaskid(long taskid) {
        this.taskid = taskid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIsDone() {
        return isDone;
    }

    public void setIsDone(String isDone) {
        this.isDone = isDone;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }
}
