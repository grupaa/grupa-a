package grupa.a.taskify.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Set;

/**
 * Encja użytkownika
 * nazwa musi być inna niż User przez nasz hosting
 */
@Entity
public class TaskifyUser extends Observable{
    /**
     * klucz główny, automatycznie generowany
     */
    @Id
    @GeneratedValue
    @Column(name = "taskifyuserid")
    private long taskifyuserid;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private byte[] password;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "salt")
    private byte[] salt;

    @OneToMany(mappedBy = "notificationid")
    private Set<Notification> notifications;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Group> groups = new ArrayList<>();

    public TaskifyUser() {
        // No argument constructor required by Hibernate
    }

    public TaskifyUser(String username, byte[] password, String name, String surname, byte[] salt) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.salt = salt;
    }

    public Set<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(Set<Notification> notifications) {
        this.notifications = notifications;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getTaskifyuserid() {
        return taskifyuserid;
    }

    public void setTaskifyuserid(long taskifyuserid) {
        this.taskifyuserid = taskifyuserid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void addGroup(Group group) {
        this.groups.add(group);
        setChanged();
        notifyObservers();
    }
}
