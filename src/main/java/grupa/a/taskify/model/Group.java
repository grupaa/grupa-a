package grupa.a.taskify.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Kazik on 18.03.2016.
 */
@Entity
@Table(name = "TaskifyGroups")
public class Group {
    @Id
    @GeneratedValue

    @Column(name = "groupid")
    private long groupid;

    @Column(name = "groupname")
    private String groupname;

    @OneToMany(mappedBy = "group")
    private Set<Board> board;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<TaskifyUser> users = new ArrayList<>();


    public Group() {
        // No argument constructor required by Hibernate
    }

    public Group(String groupname) {
        this.groupname = groupname;
    }

    public Set<Board> getBoard() {
        return board;
    }

    public void setBoard(Set<Board> board) {
        this.board = board;
    }

    public List<TaskifyUser> getUsers() {
        return users;
    }

    public void setUsers(List<TaskifyUser> users) {
        this.users = users;
    }

    public long getGroupid() {
        return groupid;
    }

    public void setGroupid(long groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }
}
