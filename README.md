# README #
** Informatyka 3 rok **
 grupa laboratoryjna 4 
###  Grupa A  

Aplikacja na przedmiot programowanie zespołowe służąca zarządzaniu zadaniami
Aplikacja jest tworzona w języku Java,, używamy następujących narzędzi:

* Apache Maven - narzędzie do automatyzacji budowy aplikacji
* JavaFX - warstwa widoku
* Hibernate - ORM
* PostgreSQL - silnik bazodanowy
* Heroku ([link](https://postgres.heroku.com/)) - hosting na bazę danych
* JFoenix ([link](https://github.com/jfoenixadmin/JFoenix)) - biblioteka do wyglądu

### Skład grupy ###

* Dawid Bartman
* Filip Cygan filipcygan@outlook.com
* Kamil Czesnowski
* Kazimierz Goździewski